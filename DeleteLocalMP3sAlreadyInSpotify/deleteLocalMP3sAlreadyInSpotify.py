__author__ = "Kim Therkelsen"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2012/05/12 $"
__copyright__ = "Free to use/modify"
__license__ = "Python"

import os
from os.path import join, isfile
import sys, json, requests, time, id3reader
            
def listDirectory(directory, fileExtList):
    "get list of file info objects for files of particular extensions"
    fileList = [os.path.normcase(f) for f in os.listdir(directory)]
    fileList = [os.path.join(directory, f) for f in fileList \
                if os.path.splitext(f)[1] in fileExtList]
    id3ObjList = []
    for f in fileList:
        try:
            id3ObjList.append((id3reader.Reader(f),f))
        except:
            print "Failed getting ID3 info for file"
    return id3ObjList

def deleteKnownMP3FilesInPath(pathToLookIn, countryCode):
    url = 'http://ws.spotify.com/search/1/track.json?q='
    for info,f in listDirectory(pathToLookIn, [".mp3"]):
        try:
            searchUrl = url
            #search for artist/track in Spotify database, like
            #http://ws.spotify.com/search/1/album.json?q="the rising"+artist:"bruce springsteen"
            #http://ws.spotify.com/search/1/track.json?q="streets of philadelphia"+artist:"bruce springsteen"+album:"Greatest Hits"
            #http://ws.spotify.com/search/1/track.json?q=artist:"bruce springsteen"+album:"Greatest Hits"+track:"streets of philadelphia"
            #By playing around with the Spotify windows application and MP3 ID tags I have noticed that
            #if the artist and title can be recognized by Spotify it does not matter if the album
            #can't. Spotify will just find a matching album.
            #I accept that the right album might not always be found and always just search for the artist/title combination
            #It is important to also check if the response territories section contains "worldwide" or the wanted countryCode  
            if (info.getValue('title')==None or info.getValue('performer')==None):
                print "MISSING title/artist for: "+f
                continue
            #For some reason spotify url must not contain &
            #Perhaps there is a special escape character for special characters but I have not found it yet 
            
            searchUrl += "+artist:"+"\""+info.getValue('performer')+"\""
            searchUrl += "+title:"+"\""+info.getValue('title')+"\""
            searchUrl = searchUrl.replace("&", "and")
            print "Trying: "
            print searchUrl  
            resp = requests.get(url=searchUrl)
            if resp.text==None:
                print "NO RESPONSE RECEIVED"
                continue
            data = json.loads(resp.text)
        except Exception, e:
            print "Exception occurred (skipping track):"
            print unicode(e.message).encode("utf-8")
            continue
        bTrackFound=False
        if data["info"]["num_results"]:
            #we also need to check if the album is available in our country or worldwide
            for track in data["tracks"]:
                territories = track["album"]["availability"]["territories"]
                if ( (territories.find(countryCode) != -1) or (territories.find("worldwide") != -1) ):
                    bTrackFound=True
                    break
            if bTrackFound:  
                try:
                    print "DELETING "+f
                    os.remove(f)
                except:
                    print "Could not delete file!!!" 
        #we are not allowed to make more than 10 requests per second
        #sleeping about 100ms plus the time spend making the request should make sure we don't exceed that limit
        time.sleep(0.1)
        
def deleteEmptyDirectories(topDirectory):
    isEmpty=True
    for entry in os.listdir(topDirectory):
        try:
            if isfile(join(topDirectory,entry))==True:
                isEmpty = False
            else:
                subEmpty = deleteEmptyDirectories(join(topDirectory, entry))
                if subEmpty==True:
                    try:
                        print "Trying to remove %s" %join(topDirectory, entry)
                        os.removedirs(join(topDirectory, entry))
                    except:
                        print #silently fail - often the directory has already been deleted because this was the parent directory
                else:
                    isEmpty=False
        except :
            print ("error checking: "+entry)
    return isEmpty

if __name__ == "__main__":
    #look in current directory and all subdirectories
    rootdir = sys.argv[1]
    countryCode = sys.argv[2]
    deleteKnownMP3FilesInPath(rootdir, countryCode)
    for root, directories, files in os.walk(rootdir):
        for subdirectory in directories:
            subDirPath = os.path.realpath(os.path.join(root, subdirectory))
            deleteKnownMP3FilesInPath(subDirPath, countryCode)
    deleteEmptyDirectories(rootdir)